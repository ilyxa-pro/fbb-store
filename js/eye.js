document.onmousemove = function (event) {
	var centerEyeX= $('#eye').offset().left + ($('#eye').width() / 2), //Координата X центра глаза
	centerEyeY= $('#eye').offset().top + ($('#eye').height() / 2), //Координата Y центра глаза
	w1 = centerEyeX/100, //1 процент от цетра по X
	h1 = centerEyeY/100, //1 процент от цетра по Y
	MouseX = event.pageX,
	MouseY = event.pageY,
	windowWidth = $( window ).width(),
	windowHeight = $( window ).height(),
	radius = $('#eye').width() / 2 - $('#pupil').width() / 2;
	// 1
	if (MouseX < centerEyeX && MouseY < centerEyeY) {
		var rangeX = Math.abs((MouseX-centerEyeX) / w1);
		var rangeY = Math.abs((MouseY-centerEyeY) / h1);
		var left = (radius/100)*(rangeX);
		var top = (radius/100)*(rangeY);
		var a = Math.sqrt((top*top)+(left*left));
		if ( a > radius) {
			var Mx = MouseX + centerEyeX;
			var My = MouseY + centerEyeY;
			var prop = Math.sqrt(((Mx*Mx)+(My*My)))/radius;
			top = Mx * (1/prop);
			left = My * (1/prop);
		}
		top = radius - top;
		left = radius - left;
		$('#pupil').css({top:top,left:left});
	}
	//2
	if (MouseX > centerEyeX && MouseY < centerEyeY) {
		var rangeX = Math.abs((MouseX-centerEyeX) / w1);
		var rangeY = Math.abs((MouseY-centerEyeY) / h1);
		var left = (radius/100)*(rangeX);
		var top = (radius/100)*(rangeY);
		var a = Math.sqrt((top*top)+(left*left));
		if ( a > radius) {
			var Mx = MouseX - centerEyeX;
			var My = centerEyeY - MouseY;
			var prop = Math.sqrt(((Mx*Mx)+(My*My)))/radius;
			console.log(prop);
			top = My * (1/prop);
			left = Mx * (1/prop);
		}
		top = radius - top;
		left = radius + left;
		$('#pupil').css({top:top,left:left});
	}
	//3
	if (MouseX > centerEyeX && MouseY > centerEyeY) {
		var rangeX = Math.abs((MouseX-centerEyeX) / w1);
		var rangeY = Math.abs((MouseY-centerEyeY) / h1);
		var left = (radius/100)*(rangeX);
		var top = (radius/100)*(rangeY);
		var a = Math.sqrt((top*top)+(left*left));
		if ( a > radius) {
			var Mx = MouseX - centerEyeX;
			var My = MouseY - centerEyeY;
			var prop = Math.sqrt(((Mx*Mx)+(My*My)))/radius;
			console.log(prop);
			top = My *(1/prop);
			left = Mx *(1/prop);
		}
		top = radius + top;
		left = radius + left;
		$('#pupil').css({top:top,left:left});
	}
	//4
	if (MouseX < centerEyeX && MouseY > centerEyeY) {
		var rangeX = Math.abs((MouseX-centerEyeX) / w1);
		var rangeY = Math.abs((MouseY-centerEyeY) / h1);
		var left = (radius/100)*(rangeX);
		var top = (radius/100)*(rangeY);
		var a = Math.sqrt((top*top)+(left*left));
		if ( a > radius) {
			var Mx = centerEyeX - MouseX;
			var My = MouseY - centerEyeY;
			var prop = Math.sqrt(((Mx*Mx)+(My*My)))/radius;
			console.log(prop);
			top = My * (1/prop);
			left = Mx * (1/prop);
		}
		top = radius + top;
		left = radius - left;
		$('#pupil').css({top:top,left:left});
	}
}
















