
function SearchList($array,Value) {
	for (var key in $array[1]) { // Забираем список свойств
		var list = $array.find(function (curr) {
				return curr[key] === Value});
		if (list != undefined) {
			return list;
		}
	}
}

function convertCurrency(inputCurrencyID,item,outputCurrency,$currency) {
	var GalacticCredits = SearchList($currency,outputCurrency);
	var CurensyUSD = SearchList($currency,inputCurrencyID);
	return Math.round(item * (CurensyUSD.Value / CurensyUSD.Nominal) / (GalacticCredits.Value / GalacticCredits.Nominal));
}

function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		vars[key] = value;
	});
	return vars
}
var id = getUrlVars()["id"];
if (id === undefined) {
	id = 'fbb-0001';
}

bookInfo.controller('bookInfo', function ($scope, $http) {
	$http.get('http://university.netology.ru/api/book').success(function(data) {
		$scope.books = data;
		$scope.maxBook = $scope.books.length;
		$scope.numLimit = 8;
		$scope.letterLimit = 4;
		$http.get('http://university.netology.ru/api/book/'+id).success(function(data) {
			$scope.booksInfo = data;
			if ( $scope.booksInfo.description == "") {
				$('.description').addClass('invisible');;
			}
			$http.get('http://university.netology.ru/api/currency').success(function(data) {
				$scope.Currency = data;
				$scope.booksInfo.priceTo = convertCurrency($scope.booksInfo.currency,$scope.booksInfo.price,'ZZZ',$scope.Currency);
				$http.get('http://university.netology.ru/api/order/delivery').success(function(data) {
					$scope.delivery = data;
					$http.get('http://university.netology.ru/api/order/payment').success(function(data) {
						$scope.payment = data;
						//Узнаем цены доставки
						for ( i = 0, max = $scope.payment.length; i <= max ; i++ ) {
							$scope.delivery[i].priceTo = convertCurrency($scope.delivery[i].currency,$scope.delivery[i].price,'ZZZ',$scope.Currency);
							if ( $scope.delivery[i].priceTo == 0 ) {
								$scope.delivery[i].priceTo = "Бесплатно";
							}
						}
						$('.form-group label input.deliveryid').on('click', function(event) {
							console.log(event.target.id);
							var targetDelivery = event.target.id;
							$http.get('http://university.netology.ru/api/order/delivery/'+targetDelivery+'/payment').success(function(data) {
								$scope.PaymentTo = data;
								$scope.payment = $scope.PaymentTo;
								$DeliveryTargetArray = $scope.delivery.find(function (curr) {
								return curr.id === targetDelivery});
								$scope.needAdress = $DeliveryTargetArray.needAdress;
							});
							//{Плюсуем к стоимости книги стоимость доставки
							$http.get('http://university.netology.ru/api/order/delivery').success(function(data) {
								var deliveryID = $(event.target).attr('id');
								$scope.booksInfo.deliveryPriceTarget = $scope.delivery.find(function (curr) {
								return curr.id === deliveryID}).priceTo;
								if ( $scope.booksInfo.deliveryPriceTarget == 'Бесплатно' ) {
									$scope.booksInfo.deliveryPriceTarget = 0;
								}
							});
							//}
						});
					});
				});
			});
		});
	});
	$scope.order = {
		payment: {},
		delivery: {}
	};
	$scope.saveOrder = function (order) {
		event.preventDefault();
		$scope.order.book = id;
		$scope.order.manager = "ilyxaang@gmail.com";
		$scope.order.payment.currency = "R01589";
		console.log(order);
		var jqxhr = $.post("http://university.netology.ru/api/order", order);
		jqxhr.done(function () {
			console.log("Данные отправлены на сервер");
			var url= 'done.html?id='+id;
			//window.location =url;
		});
		jqxhr.fail(function () {
			console.log(jqxhr.responseText);
			console.log("Ошибка отправки данных");
		});
	};
});